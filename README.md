# Draw a circle using WebAssembly w/ TypeScript support

## Installation Requirements
```bash
# Install Emscripten
# - used to compile C to WebAssembly
# - install instructions: http://kripken.github.io/emscripten-site/

# Install Node & Node Package Manager (npm)
# - used to build the project (follow instructions below)
# - install instructions: https://nodejs.org/en/
```

## 🔨 Build Setup

```bash
# install dependencies
npm install

# start server at localhost:8080
npm run start

# build for production
npm run build
```

## Tools Used
* WebAssembly
* TypeScript
