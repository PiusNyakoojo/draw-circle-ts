var path = require('path')

module.exports = {
  entry: path.resolve(__dirname, '../dist/lib/index.js'),
  output: {
    path: path.resolve(__dirname, '../dist/lib'),
    filename: 'index.js'
  }
}
