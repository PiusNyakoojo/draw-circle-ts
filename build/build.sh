#!/bin/bash

printf 'Clearing /dist directory.\n'
rm -rf './dist/*'

printf 'Copying index.html.\n'
cp './src/index.html' './dist/index.html'

printf '\n\nTranspiling TypeScript to JavaScript...\n'
gulp build --gulpfile './build/gulpfile.js'

printf '\n\nBundling JavaScript files...\n'
webpack --config './build/webpack.config.js'

printf '\n\nGenerating WebAssembly...\n'
emcc './src/main.c' -s WASM=1 -o './dist/a.out.js' \
  --js-library './dist/lib/index.js' \
  -s EXPORTED_FUNCTIONS='["_main"]' \
  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]'

printf '\nBuild Complete!\n'
