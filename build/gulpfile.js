var gulp = require('gulp')
var path = require('path')
var clean = require('gulp-clean')
var ts = require('gulp-typescript')
var gulpTslint = require('gulp-tslint')
var tslint = require('tslint')
var runSequence = require('run-sequence')
var replace = require('gulp-replace')
var merge = require('merge-stream')

var envConfig = require('./env')

var PATH_SRC = envConfig.PATH_SRC
var PATH_DIST = envConfig.PATH_DIST

// Watch files for changes
gulp.task('watch', function () {
    gulp.watch(`${PATH_SRC}/**/*.ts`, ['build'])
})

// Build project
gulp.task('build', function (done) {
  runSequence(
    'tslint',
    'typescript',
  function () {
    done()
  })
})

// Compile TypeScript (.ts) to JavaScript (.js)
gulp.task('typescript', function () {
  var tsProject = ts.createProject(`${PATH_SRC}/tsconfig.json`)
  return gulp.src([`${PATH_SRC}/**/*.ts`])
    .pipe(tsProject())
    .pipe(gulp.dest(`${PATH_DIST}`))
})

// Lint TypeScript files (.ts)
gulp.task('tslint', function () {
  var program = tslint.Linter.createProgram(`${PATH_SRC}/tsconfig.json`)

  return gulp.src([`${PATH_SRC}/**/*.ts`], { base: '.' })
    .pipe(gulpTslint({
      program,
      formatter: "verbose"
    }))
    .pipe(gulpTslint.report())
})
