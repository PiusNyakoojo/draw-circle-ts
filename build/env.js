var path = require('path')

module.exports = {
  PATH_SRC: path.resolve(__dirname, '../src'),
  PATH_DIST: path.resolve(__dirname, '../dist')
}
