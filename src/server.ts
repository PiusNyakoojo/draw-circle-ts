import * as path from 'path'
import * as http from 'http'
import * as express from 'express'

// Setup environment variables
const ENV_PORT = process.env.PORT || 8080

// Setup router for request traffic
let router = express()
router.use(express.static(__dirname))

// Serve index.html
router.get('/*', function (req: express.Request, res: express.Response) {
  res.sendFile(path.resolve(__dirname, './index.html'))
})

// Start listening for requests!
const server = http.createServer(router)
server.listen(ENV_PORT)
server.on('error', onServerError)
server.on('listening', onServerListen)

// Handle server error
function onServerError (err: any) {
  console.log(`Error running server: ${err}`)
}

// Handle successful server start
function onServerListen () {
  console.log(`Listening on port: ${ENV_PORT}`)
}
