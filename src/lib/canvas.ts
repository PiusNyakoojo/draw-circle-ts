// Helper for interacting with the HTML5 Canvas

let canvasHelper = {
  jsArc__postset: '_jsArc();',
  jsArc: jsArc
}

declare var _jsArc: any

// Wrapper function for Context.arc()
function jsArc (x: number, y: number, r: number, sAngle: number, eAngle: number, counterclockwise: boolean) {
  let canvas = <HTMLCanvasElement>document.getElementById('canvas')
  let ctx = canvas.getContext('2d')

  _jsArc = function (x: number, y: number, r: number, sAngle: number, eAngle: number, counterclockwise: boolean) {
    ctx.beginPath()
    ctx.arc(x, y, r, sAngle, eAngle, counterclockwise || false)
    ctx.stroke()
  } || _jsArc // prevent '_jsArc is unused' error from tslint
}

export default canvasHelper
