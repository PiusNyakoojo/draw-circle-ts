import canvasHelper from './canvas'

// Declare global variables for the typescript compiler.
// mergeInto and LibraryManager are provided by emscripten.
declare var mergeInto: any
declare var LibraryManager: any

let lib = {
  ...canvasHelper
}

mergeInto(LibraryManager.library, lib)
