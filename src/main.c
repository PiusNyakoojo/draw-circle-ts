#include <stdio.h>
#include <math.h>

typedef int bool;
#define true 1
#define false 0

extern void jsArc(int x, int y, int r, float sAngle, float eAngle, bool counterclockwise);

int main() {
  printf("Program started.\n");

  // Draw a circle
  jsArc(100, 75, 50, 0, 2 * M_PI, true);
  jsArc(100, 100, 25, 0, 2 * M_PI, true);
}
